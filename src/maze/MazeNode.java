package maze;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Hasse on 2014-09-05.
 */
public class MazeNode implements Node {
    private ArrayList<MazeNode> neighbours;
    private ArrayList<MazeNode> linkedNeighbours;
    private Random rand = new Random();

    private final int x;
    private final int y;
    private boolean visited;
    private boolean startNode;

    public MazeNode(Builder builder) {
        this.startNode = builder.startNode;
        this.x = builder.x;
        this.y = builder.y;
        visited = false;
        neighbours = new ArrayList<MazeNode>();
        linkedNeighbours = new ArrayList<MazeNode>();
    }

    @Override
    public int getX() {
        return x;
    }

    @Override
    public int getY() {
        return y;
    }

    public void markAsVisited() {
        visited = true;
    }

    public boolean isVisited() {
        return visited;
    }

    public void connectWithNeighbour(MazeNode neighbour) {
        if(!isAdjacentTo(neighbour)) {
            throw new NeighbourAssertionException();
        } else if (!linkedNeighbours.contains(neighbour)) {
            linkedNeighbours.add(neighbour);
            neighbour.connectWithNeighbour(this);
        }
    }

    public boolean hasUnvisitedNeighbour() {
        for(MazeNode neighbour : neighbours) {
            if(!neighbour.isVisited()) {
                return true;
            }
        }
        return false;
    }

    public MazeNode getRandomUnvisitedNeighbour() {
        ArrayList<MazeNode> unvisitedNeighbours = new ArrayList<MazeNode>();
        for(MazeNode neighbour : neighbours) {
            if(!neighbour.isVisited()) {
                unvisitedNeighbours.add(neighbour);
            }
        }
        if(unvisitedNeighbours.isEmpty()) {
            return null;
        } else {
            return unvisitedNeighbours.get(rand.nextInt(unvisitedNeighbours.size()));
        }
    }

    @Override
    public boolean hasPathWest() {
        for(MazeNode mazeNode : linkedNeighbours) {
            if(mazeNode.getX() == x - 1 && mazeNode.getY() == y) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean hasPathEast() {
        for(MazeNode mazeNode : linkedNeighbours) {
            if(mazeNode.getX() == x + 1 && mazeNode.getY() == y) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean hasPathNorth() {
        for(MazeNode mazeNode : linkedNeighbours) {
            if(mazeNode.getX() == x  && mazeNode.getY() == y - 1) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean hasPathSouth() {
        for(MazeNode mazeNode : linkedNeighbours) {
            if(mazeNode.getX() == x  && mazeNode.getY() == y + 1) {
                return true;
            }
        }
        return false;
    }

    private boolean isAdjacentTo(MazeNode suggestedNeighbour) {
        return Math.abs(suggestedNeighbour.getX() - x) + Math.abs(suggestedNeighbour.getY() - y) == 1;
    }

    @Override
    public boolean isStartNode() {
        return startNode;
    }


    public static class Builder {
        private int x;
        private int y;
        private boolean startNode;

        public Builder setX(int x) {this.x = x; return this;}
        public Builder setY(int y) {this.y = y; return this;}
        public Builder isStartNode(boolean isStartNode) {this.startNode = isStartNode; return this;}
        public MazeNode build() {return new MazeNode(this);}
    }

    public void declareNeighbourTo(MazeNode node) {
        if(!this.isAdjacentTo(node)) {
            throw new NeighbourAssertionException();
        } else if (!neighbours.contains(node)) {
            neighbours.add(node);
            node.declareNeighbourTo(this);
        }
    }
}
