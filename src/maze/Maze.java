package maze;

public interface Maze {
    /**
     * Gives the number of cells in one row of the maze. In other words how many cells there are in the
     * maze if you start from the left and walk all the way to the right following a straight path.
     *
     * @return the number of cells in one row of the maze.
     */
    public int getWidth();

    /**
     * Gives the number of cells in one column of the maze. In other words how many cells there are in the
     * maze if you start from the top and walk all the way to the bottom following a straight path.
     *
     * @return the number of cells in one column of the maze.
     */
    public int getHeight();

    /**
     * Checks if the specified coordinates is within the maze bounds.
     *
     * @param x the column of the node.
     * @param y the row of the node.
     * @return true if the specified coordinate is within the maze bounds, otherwise false.
     */
    public boolean withinMazeBounds(int x, int y);

    /**
     * Returns the node at the location specified by x and y.
     *
     * @throws java.lang.IllegalArgumentException if the x or y coordinate is outside of the maze bounds.
     * @throws java.lang.IllegalStateException if the maze is not yet generated.
     *
     * @param x the column of the node. Between 0 and the maze width - 1.
     * @param y the row of the node. Between 0 and the maze height - 1.
     * @return the node at the location specified.
     */
    public Node getNodeAtLocation(int x, int y);

    /**
     * Returns the entry node of the maze.
     *
     * @throws java.lang.IllegalStateException if the maze is not yet generated.
     *
     * @return the entry node of this maze.
     */
    public Node getStartNode();

    /**
     * Generates the maze. This must be called before the maze is used. Otherwise an illegal state exception
     * will be thrown when trying to access the other methods.
     *
     * @throws java.lang.IllegalStateException if the maze is already generated.
     */
    public void generate();
}