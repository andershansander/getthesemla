package maze;

public interface Node {
    int getX();

    int getY();

    boolean hasPathWest();

    boolean hasPathEast();

    boolean hasPathNorth();

    boolean hasPathSouth();

    boolean isStartNode();
}
