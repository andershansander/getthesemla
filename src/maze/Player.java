package maze;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Observable;

public class Player extends Observable {
    public Player() {
        this.path = new ArrayList<PathCoordinate>();
    }

    public static class PathCoordinate {
        private final int x;
        private final int y;

        private PathCoordinate(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public int getX() {
            return x;
        }

        public int getY() {
            return y;
        }
    }

    private final List<PathCoordinate> path;

    public void addPathCoordinate(int x, int y) {
        path.add(new PathCoordinate(x, y));
    }

    public PathCoordinate getCurrentLocation() {
        return path.get(path.size() - 1);
    }

    public PathCoordinate getLastLocation() {
        return path.get(path.size() - 2);
    }
}
