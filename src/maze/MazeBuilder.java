package maze;

public class MazeBuilder {
    private MazeBuilder() {}

    public static Maze generateQuadraticMaze(int dimensions) {
        QuadraticMaze maze = new QuadraticMaze(dimensions);
        maze.generate();
        return maze;
    }
}
