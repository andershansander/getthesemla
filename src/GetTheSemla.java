import controllers.DefaultPlayerController;
import controllers.GameController;
import gamelogic.Game;
import javafx.application.Application;
import javafx.stage.Stage;
import views.GameWindow;

public class GetTheSemla extends Application {

    public static void main(String[] args) {
        launch();
    }

    @Override
    public void start(Stage stage) throws Exception {
        final Game game = new Game(GameWindow.MAZE_CELL_SIZE);
        final GameController controller = new GameController(new DefaultPlayerController(game), game);
        new GameWindow(stage, controller, game);
    }
}