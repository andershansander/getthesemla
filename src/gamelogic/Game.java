package gamelogic;

import maze.Maze;
import maze.MazeBuilder;
import maze.Player;

import java.util.*;

public class Game {
    private static final double MAX_SCORE = 1000;
    private static final int MAZE_DIMENSIONS = 20;

    private final int mazeCellSizeInPixels;

    private Maze maze;
    private Player player;

    private boolean gameRunning;
    private boolean semlaFound;
    private boolean enteredMaze;

    private long startTime;
    private double topScore;
    private double lastScore;
    private double currentScore;
    private Map<Integer, List<Double>> playerScores;
    private boolean versusMode = false;

    private int currentPlayerNumber = 1;

    public Game(int mazeCellSizeInPixels) {
        this.mazeCellSizeInPixels = mazeCellSizeInPixels;
        playerScores = new HashMap<Integer, List<Double>>();
        playerScores.put(1, new ArrayList<Double>());
        playerScores.put(2, new ArrayList<Double>());
        topScore = 0;
        maze = MazeBuilder.generateQuadraticMaze(MAZE_DIMENSIONS);
    }

    public void initializeNewGame() {
        maze = MazeBuilder.generateQuadraticMaze(MAZE_DIMENSIONS);
        currentScore = 0;
        startTime = System.currentTimeMillis();
        gameRunning = true;
        semlaFound = false;
        enteredMaze = false;
        player = new Player();
    }

    public void updateGame() {
        if(gameRunning) {
            currentScore = calculateScore();

            if(!enteredMaze) {
                if(maze.getStartNode().getX() == getCurrentCellX() && maze.getStartNode().getY() == getCurrentCellY()) {
                    enteredMaze = true;
                } else if(getCurrentCellX() >= 0 && getCurrentCellY() >= 0 && getCurrentCellX() < maze.getWidth() && getCurrentCellY() < maze.getHeight()) {
                    //player entered the maze in the wrong place
                    onPlayerDied();
                }
            }
            else if(collidedWithWall()) {
                onPlayerDied();
            }
            else if(gotSemla()) {
                onPlayerWon();
            }
        }
    }

    private int getCurrentCellX() {
        return player.getCurrentLocation().getX() / mazeCellSizeInPixels;
    }

    private int getCurrentCellY() {
        return player.getCurrentLocation().getY() / mazeCellSizeInPixels;
    }

    private int getLastCellX() {
        return player.getLastLocation().getX() / mazeCellSizeInPixels;
    }

    private int getLastCellY() {
        return player.getLastLocation().getY() / mazeCellSizeInPixels;
    }

    private boolean collidedWithWall() {
        int currentCellX = Math.min(Math.max(0, getCurrentCellX()), maze.getWidth() - 1);
        int currentCellY = Math.min(Math.max(0, getCurrentCellY()), maze.getHeight() - 1);
        int lastCellX = Math.min(Math.max(0, getLastCellX()), maze.getWidth() - 1);
        int lastCellY = Math.min(Math.max(0, getLastCellY()), maze.getHeight() - 1);

        //we have moved east and there is no path east
        if(currentCellX > lastCellX && !maze.getNodeAtLocation(lastCellX, lastCellY).hasPathEast()) {
            return true;
        }
        //we have moved west and there is no path west
        if(currentCellX < lastCellX && !maze.getNodeAtLocation(lastCellX, lastCellY).hasPathWest()) {
            return true;
        }
        //we have moved north and there is no path north
        if(currentCellY < lastCellY && !maze.getNodeAtLocation(lastCellX, lastCellY).hasPathNorth()) {
            return true;
        }
        //we have moved south and there is no path south
        if(currentCellY > lastCellY && !maze.getNodeAtLocation(lastCellX, lastCellY).hasPathSouth()) {
            return true;
        }

        //we have not collided with anything
        return false;
    }

    private boolean gotSemla() {
        return getCurrentCellX() == maze.getWidth() / 2 && getCurrentCellY() == maze.getHeight() / 2;
    }

    private void switchCurrentPlayer() {
        if(currentPlayerNumber == 1) {
            currentPlayerNumber = 2;
        } else {
            currentPlayerNumber = 1;
        }
    }

    private void onPlayerWon() {
        if(versusMode) {
            playerScores.get(currentPlayerNumber).add(currentScore);
            switchCurrentPlayer();
        }

        semlaFound = true;
        lastScore = currentScore;
        if(currentScore > topScore) {
            topScore = currentScore;
        }
        gameRunning = false;
    }

    private void onPlayerDied() {
        if(versusMode) {
            playerScores.get(currentPlayerNumber).add(0d);
            switchCurrentPlayer();
        }
        lastScore = currentScore = 0;
        gameRunning = false;
    }

    private double calculateScore() {
        return Math.max(1, MAX_SCORE / ((double)(System.currentTimeMillis() - startTime) / 1000));
    }

    public Maze getCurrentMaze() {
        return maze;
    }

    public long getCurrentScore() {
        return (long) currentScore;
    }

    public boolean isGameRunning() {
        return gameRunning;
    }

    public long getTopScore() {
        return (long) topScore;
    }

    public long getLastScore() {
        return (long) lastScore;
    }

    public boolean isSemlaFound() {
        return semlaFound;
    }

    public void setVersusMode(boolean versusMode) {
        this.versusMode = versusMode;
        if(!versusMode) {
            resetVersusMode();
        }
    }

    private void resetVersusMode() {
        currentPlayerNumber = 1;
        playerScores.get(1).clear();
        playerScores.get(2).clear();
    }

    public double getTotalScoreForPlayer(int player) {
        double sum = 0d;

        for(Double score : playerScores.get(player)) {
            sum += score;
        }
        return sum;
    }

    public Player getCurrentPlayer() {
        return player;
    }

    public boolean isInStartArea(int x, int y) {
        return x < MAZE_DIMENSIONS * mazeCellSizeInPixels && y > MAZE_DIMENSIONS * mazeCellSizeInPixels;
    }
}