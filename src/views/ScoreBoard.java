package views;

import controllers.GameController;
import javafx.event.EventHandler;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

public class ScoreBoard extends VBox {
    private Label bestScore;
    private double bestScoreValue;
    private Label lastScore;
    private Label currentScore;
    private Label player1sScore;
    private Label player2sScore;
    private Label currentPlayerLabel;

    private final Font FONT = Font.font("Arial", 36d);
    private final Font SMALL_FONT = Font.font("Arial", 16d);

    public ScoreBoard(final GameController gameController) {
        currentScore = new Label("Score: ---");
        currentScore.setFont(FONT);
        lastScore = new Label("Last score: ---");
        lastScore.setFont(FONT);
        bestScore = new Label("Best score: ---");
        bestScore.setFont(FONT);
        currentPlayerLabel = new Label("Current player: ---");
        currentPlayerLabel.setFont(FONT);
        player1sScore = new Label("Player 1s score: ---");
        player1sScore.setFont(FONT);
        player2sScore = new Label("Player 2s score: ---");
        player2sScore.setFont(FONT);

        Label startInformation = new Label("Click under the maze to begin.");
        startInformation.setFont(SMALL_FONT);
        Label emptyRow = new Label("");
        emptyRow.setFont(FONT);

        final CheckBox versusMode = new CheckBox("Versus mode");
        versusMode.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                gameController.setVersusMode(versusMode.isSelected());
            }
        });

        this.getChildren().addAll(currentScore, lastScore, bestScore, emptyRow, startInformation, versusMode, currentPlayerLabel, player1sScore, player2sScore);
    }

    public void updateCurrentScore(double score) {
        currentScore.setText("Score: " + score);
    }

    public void updateLastScore(double score) {
        lastScore.setText("Last score: " + score);
    }

    public void updateTopScore(double score) {
        bestScoreValue = score;
        bestScore.setText("Best score: " + bestScoreValue);
    }

    public double getTopScore() {
        return bestScoreValue;
    };

    public void updateCurrentPlayer(int currentPlayerNumber) {
        currentPlayerLabel.setText("Current player: Player " + currentPlayerNumber);
    }

    public void updatePlayerOnesScore(double score) {
        player1sScore.setText("Player 1s score: " + score);
    }

    public void updatePlayerTwosScore(double score) {
        player2sScore.setText("Player 2s score: " + score);
    }
}
