package views;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import maze.Maze;

public class MazePrinter {
    GraphicsContext graphicsContext;

    public MazePrinter(GraphicsContext graphicsContext) {
        this.graphicsContext = graphicsContext;
    }

    public void printMaze(Maze maze, int mazeX, int mazeY, int cellSize, Color color) {

        graphicsContext.setLineWidth(3);
        graphicsContext.setStroke(color);

        for(int x = 0; x < maze.getWidth(); x++) {
            int cellX = mazeX + x * cellSize;

            for(int y = 0; y < maze.getHeight(); y++) {
                int cellY = mazeY + y * cellSize;
                if (!maze.getNodeAtLocation(x, y).hasPathNorth()) {
                    graphicsContext.strokeLine(cellX, cellY, cellX + cellSize, cellY);
                }
                if (!maze.getNodeAtLocation(x, y).hasPathEast()) {
                    graphicsContext.strokeLine(cellX + cellSize, cellY, cellX + cellSize, cellY + cellSize);
                }
                if (!maze.getNodeAtLocation(x, y).hasPathSouth() && !maze.getNodeAtLocation(x, y).isStartNode()) {
                    graphicsContext.strokeLine(cellX, cellY + cellSize, cellX + cellSize, cellY + cellSize);
                }
                if (!maze.getNodeAtLocation(x, y).hasPathWest()) {
                    graphicsContext.strokeLine(cellX, cellY, cellX, cellY + cellSize);
                }
            }
        }
    }
}
