package views;

import controllers.GameController;
import gamelogic.Game;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import maze.Player;

public class GameWindow {
    public final static int MAZE_CELL_SIZE = 35;

    private final static int GAME_AREA_MARGIN = 50;
    private final static int GAME_AREA_WIDTH = 800;
    private final static int GAME_AREA_HEIGHT = 800;
    private final static int WINDOW_WIDTH = 1200;
    private final static int WINDOW_HEIGHT = 800;

    private final static Color BACKGROUND_COLOR = Color.MEDIUMORCHID;
    private final static Color MAZE_WALL_COLOR = Color.DARKSALMON;
    private final static Color PATH_COLOR = Color.CYAN;
    private static final Color COLLISION_PATH_COLOR = Color.RED;
    private static final Color START_AREA_COLOR = Color.PEACHPUFF;
    private static final String WINDOW_TITLE = "Get the Semla!";

    private final GameController gameController;
    private final Game game;

    //GUI components
    private ScoreBoard scoreBoard;
    private GraphicsContext graphicsContext;
    private MazePrinter mazePrinter;
    private final static Image SEMLA = new Image(GameWindow.class.getResource("images/semla.jpg").toString());

    private MusicPlayer musicPlayer;
    private int lastMazeHashCode;

    public GameWindow(Stage mainStage, GameController gameController, Game game) {
        this.gameController = gameController;
        this.game = game;
        mainStage.setTitle(WINDOW_TITLE);
        musicPlayer = new MusicPlayer();
        HBox root = new HBox();
        initGUIComponents(root);
        mainStage.setScene(new Scene(root, WINDOW_WIDTH, WINDOW_HEIGHT));
        mainStage.show();
    }

    private void initGUIComponents(Pane root) {
        Canvas canvas = new Canvas(GAME_AREA_WIDTH, GAME_AREA_HEIGHT);
        graphicsContext = canvas.getGraphicsContext2D();
        mazePrinter = new MazePrinter(graphicsContext);

        canvas.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                gameController.handleMouseClick(mouseEvent.getX() - GAME_AREA_MARGIN, mouseEvent.getY() - GAME_AREA_MARGIN);
                draw();
            }
        });

        canvas.addEventHandler(MouseEvent.MOUSE_MOVED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                gameController.handleMouseMoved(mouseEvent.getX() - GAME_AREA_MARGIN, mouseEvent.getY() - GAME_AREA_MARGIN);
                draw();
            }
        });
        scoreBoard = new ScoreBoard(gameController);
        root.getChildren().addAll(canvas, scoreBoard);
    }

    private void draw() {
        if(game.getCurrentMaze().hashCode() != lastMazeHashCode) {
            graphicsContext.setFill(BACKGROUND_COLOR);
            graphicsContext.fillRect(0, 0, GAME_AREA_WIDTH, GAME_AREA_HEIGHT - GAME_AREA_MARGIN);
            graphicsContext.drawImage(SEMLA, GAME_AREA_MARGIN + game.getCurrentMaze().getWidth() / 2 * MAZE_CELL_SIZE, GAME_AREA_MARGIN + game.getCurrentMaze().getHeight() / 2 * MAZE_CELL_SIZE);

            graphicsContext.setFill(START_AREA_COLOR);
            graphicsContext.fillRect(0, GAME_AREA_HEIGHT - GAME_AREA_MARGIN, GAME_AREA_WIDTH, GAME_AREA_HEIGHT);
            mazePrinter.printMaze(game.getCurrentMaze(), GAME_AREA_MARGIN, GAME_AREA_MARGIN, MAZE_CELL_SIZE, MAZE_WALL_COLOR);
            lastMazeHashCode = game.getCurrentMaze().hashCode();
        }
        if(game.isGameRunning() && !musicPlayer.isPlaying()) {
            musicPlayer.startRandomMusic();
        }

        if(!game.isGameRunning() && !game.isSemlaFound()) {
            graphicsContext.setStroke(COLLISION_PATH_COLOR);
            musicPlayer.stopMusic();
        } else {
            graphicsContext.setStroke(PATH_COLOR);
        }

        if(game.isGameRunning()) {
            final Player.PathCoordinate newLocation = game.getCurrentPlayer().getCurrentLocation();
            final Player.PathCoordinate lastLocation = game.getCurrentPlayer().getLastLocation();
            graphicsContext.strokeLine(newLocation.getX() + GAME_AREA_MARGIN, newLocation.getY() + GAME_AREA_MARGIN, lastLocation.getX() + GAME_AREA_MARGIN, lastLocation.getY() + GAME_AREA_MARGIN);
        }

        updateScores();
    }

    private void updateScores() {
        scoreBoard.updateCurrentScore(game.getCurrentScore());
        if(game.getLastScore() > scoreBoard.getTopScore()) {
            scoreBoard.updateTopScore(game.getTopScore());
        }
        if(!game.isGameRunning()) {
            scoreBoard.updateLastScore(game.getLastScore());
        }

        scoreBoard.updatePlayerOnesScore(game.getTotalScoreForPlayer(1));
        scoreBoard.updatePlayerTwosScore(game.getTotalScoreForPlayer(2));
    }
}
