package views;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

import java.io.File;
import java.io.FilenameFilter;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MusicPlayer {
    private List<MediaPlayer> mediaPlayers;
    private MediaPlayer currentMediaPlayer;

    public MusicPlayer() {
       File dir;
        try {
            dir = new File(MusicPlayer.class.getResource("/views/sounds").toURI());
        } catch (URISyntaxException e) {
            throw new AssertionError("Should never get here.", e);
        }
        mediaPlayers = new ArrayList<MediaPlayer>();
        for (String file : dir.list(new FilenameFilter() {
            @Override public boolean accept(File dir, String name) {
                return name.endsWith(".mp3");
            }
        })) {
            final MediaPlayer player = new MediaPlayer(new Media("file:///" + (dir + "\\" + file).replace("\\", "/").replaceAll(" ", "%20")));
            mediaPlayers.add(player);
        }
    }

    public void startRandomMusic() {
        stopMusic();

        currentMediaPlayer = mediaPlayers.get(new Random(System.currentTimeMillis()).nextInt(mediaPlayers.size()));
        currentMediaPlayer.play();
    }

    public void stopMusic() {
        if (currentMediaPlayer != null) {
            currentMediaPlayer.stop();
            currentMediaPlayer = null;
        }
    }

    public boolean isPlaying() {
        return currentMediaPlayer != null;
    }
}
