package controllers;

public interface PlayerController {
    public void handleMouseMoved(double x, double y);
    public void handleMouseClick(double clickX, double clickY);
}
