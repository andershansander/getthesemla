package controllers;

import gamelogic.Game;

/**
 * Extension of a PlayerController. Also has methods to deal with user input outside of the game, like menus, etc...
 */
public class GameController implements PlayerController {
    private final PlayerController playerController;
    private final Game game;

    public GameController(PlayerController playerController, Game game) {
        this.playerController = playerController;
        this.game = game;
    }

    @Override
    public void handleMouseMoved(double x, double y) {
        playerController.handleMouseMoved(x, y);
        game.updateGame();
    }

    @Override
    public void handleMouseClick(double clickX, double clickY) {
        playerController.handleMouseClick(clickX, clickY);
        game.updateGame();
    }

    public void setVersusMode(boolean versusMode) {
        game.setVersusMode(versusMode);
    }
}
