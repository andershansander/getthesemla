package controllers;

import gamelogic.Game;

public class DefaultPlayerController implements PlayerController{
    private final Game game;

    public DefaultPlayerController(Game game) {
        this.game = game;
    }

    @Override
    public void handleMouseMoved(double x, double y) {
        if(game.isGameRunning()) {
            game.getCurrentPlayer().addPathCoordinate(Double.valueOf(x).intValue(), Double.valueOf(y).intValue());
        }
    }

    @Override
    public void handleMouseClick(double x, double y) {
        final int x1 = Double.valueOf(x).intValue();
        final int y1 = Double.valueOf(y).intValue();

        if(game.isInStartArea(x1, y1) && !game.isGameRunning()) {
            game.initializeNewGame();

            //add two points so the player has one current and one last location
            game.getCurrentPlayer().addPathCoordinate(x1, y1);
            game.getCurrentPlayer().addPathCoordinate(x1, y1);
        }
    }
}
